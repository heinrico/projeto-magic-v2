var requisicao = new XMLHttpRequest();
requisicao.open("GET", "https://api.magicthegathering.io/v1/cards", true);
 
requisicao.onload = function() {
 
  if (requisicao.status >= 200 && requisicao.status < 400) {
 
    var data = JSON.parse(requisicao.responseText);
    console.log(data.cards);
 
 
 
    data.cards.forEach(function(carta, index){
      console.log("Carta: " + carta.name);
      console.log("Raridade: " + carta.rarity);
      console.log("Imagem: " + carta.imageUrl);
      let n = Math.floor(index / 4);
      if(index % 4 == 0) {
        let nova_row = document.createElement('div');
        nova_row.setAttribute('class', 'row');
        nova_row.setAttribute('id', 'cartas_' + n);
        section_cartas = document.getElementById('cartinhas');
        section_cartas.appendChild(nova_row);
      }
      let carta1 = document.getElementById('cartas_' + n);
      let one = document.createElement('div');
      one.setAttribute('class', 'col-md-3');
      let did = document.createElement('img');
      did.setAttribute('class', 'imagem img-responsive');
      if (carta.imageUrl)
        did.setAttribute('src', carta.imageUrl);
      else
        did.setAttribute('src', 'http://www.magiclibrarities.net/images/edition_comment/581.jpg');
      one.appendChild(did);
      let div = document.createElement('div');
      div.setAttribute('class', 'nome');
      let stop = document.createElement('p');
      let texto = document.createTextNode(carta.name);
      stop.appendChild(texto);
      div.appendChild(stop);
 
      one.appendChild(div);
      let raridade = document.createElement('div');
      raridade.setAttribute('class', 'rarity');
      let par = document.createElement('p');
      let texto2 = document.createTextNode(carta.rarity);
      par.appendChild(texto2);
      raridade.appendChild(par);
      one.appendChild(raridade);
      carta1.appendChild(one);
    });
  } else {
 
 
  }
};
 
requisicao.onerror = function() {
 
};
 
//Essa é a chamada que envia a requisição de fato.
requisicao.send();